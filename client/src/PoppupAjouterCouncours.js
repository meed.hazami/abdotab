import React, { useState , useEffect} from "react";
import axios from "axios";
import { v4 as uuidv4 } from 'uuid';
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import OutlinedInput from '@mui/material/OutlinedInput';
import Chip from '@mui/material/Chip';


export default function PoppupAjouterCouncours({ openPopup, open }) {
  const theme = useTheme();
  const [nom, setNom] = useState("");
  const [liberation, setLiberation] = useState("");
  const [pigeonId, setPigeonId] = useState([]);
  const [N, setN] = useState("");
  const [E, setE] = useState("");
  const [id, setId] = useState(uuidv4());

  const [nom1, setNom1] = useState({});


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};


function getStyles(name , pigeonId , theme ) {
  return {
    fontWeight:
    pigeonId.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}



  const [appState, setAppState] = useState({
    loading: false,
    repos: null,
  });

  useEffect(() => {
    setAppState({ loading: true });
    const apiUrl = `/api/admin/getpigeon`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState({ loading: false, repos: repos });
      });
  }, [setAppState]);


  const handleConcours = async (e) => {
    let now = new Date(liberation);
    let time  = new Date(now.setHours(now.getHours() + 1));
    let dataPigeon  = pigeonId.map(el => ({ name: el , dateArriver : null}))
    e.preventDefault();
      await axios
        .request({
          url: "/api/admin/concours",
          method: "POST",
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          data: {
            id,
            nom,
            liberation : time,
            pigeonId : dataPigeon,
            N,
            E
          }
        })
        .then((res) => openPopup(false))
        .catch((err) => console.log(err))
  };



  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setPigeonId(
      // On autofill we get a stringified value.
      typeof value  === 'string' ? value.split(',') : value,
    );


  };

  return (
    <Modal

      open={open}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <div className="poppupC">
        <div className="poppup-title">
          <h4>Ajouter</h4>
          <div className="poppup-close" onClick={() => openPopup(false)}>
            x
          </div>
        </div>
        <div className="poppup-body" >
          <div className="poppup-body-box1">
          <h3>Nom :</h3>
          <input
            type="text"
            onChange={(e) => setNom(e.target.value)}
            name="nom"
            value={nom}
            className="input-box"
            placeholder="Nom"
          />
          </div>

          <div className="poppup-body-box1">
          <h3>liberation :</h3>
          <input
            type="datetime-local"
            name="liberation"
            value={liberation}
            className="input-box"
            placeholder="liberation"
            onChange={(e) => setLiberation(e.target.value)}
            step="1"
            pattern="\d{2}-\d{2}-\d{4}T\d{2}:\d{2}:\d{2}"
          />
          </div>

          <div className="poppup-body-box1">
          <h3>Nomber pigeon :</h3>
          <FormControl sx={{ m: 0, width: 360 }}>
        <InputLabel id="demo-multiple-chip-label">Chip</InputLabel>
        <Select
          labelId="demo-multiple-chip-label"
          id="demo-multiple-chip"
          multiple
          value={pigeonId}
          onChange={handleChange}
          input={<OutlinedInput   id="select-multiple-chip" label="Chip" />}
          renderValue={(selected) => (
            <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
              {selected.map((value) => (
                <Chip key={value} label={value} />
              ))}
            </Box>
          )}
          MenuProps={MenuProps}
        >
            {appState.repos &&
                      appState.repos.message.map((val, index) => (
                        <MenuItem key={index} value={val.pigeon} style={getStyles(index, pigeonId, theme)}>
                          {val.pigeon} {"-"} {val.annee}
                        </MenuItem>
                    ))
            }
        </Select>
      </FormControl>

          </div>

          <div className="poppup-body-box1">
          <h3>N :</h3>
          <input
            type="text"
            name="N"
            value={N}
            className="input-box"
            placeholder="N"
            onChange={(e) => setN(e.target.value)}
          />
          </div>

          <div className="poppup-body-box1">
          <h3>E :</h3>
          <input
            type="text"
            name="E"
            value={E}
            className="input-box"
            placeholder="E"
            onChange={(e) => setE(e.target.value)}
          />
          </div>

          <div className="button-enrgister">
        <Button
          onClick={handleConcours}
          style={{ width: "200px", backgroundColor: "#306ED9" , color : "#fff"}}
          sx={{ margin: "0 5px 10px 0" }}
          // variant="contained"
        >
          Enregister
        </Button>
          </div>

        </div>
        </div>
    </Modal>
  );
}
