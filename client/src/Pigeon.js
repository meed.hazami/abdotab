import React , {useState  , useEffect} from "react";
import Button from "@mui/material/Button";
import PoppupAjourer from "./PoppupAjouterPigeon";

export default function Pigeon() {
  const [poppupAjouter, setPoppupAjouter] = useState(false);

  const [appState, setAppState] = useState({
    loading: false,
    repos: null,
  });

  const [appState1, setAppState1] = useState({
    loading: false,
    repos: null,
  });


  useEffect(() => {
    setAppState({ loading: true });
    const apiUrl = `/api/admin/getpigeon`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState({ loading: false, repos: repos });
      });
  }, [setAppState, poppupAjouter, setPoppupAjouter]);

  useEffect(() => {
    setAppState1({ loading: true });
    const apiUrl = `/api/admin/getmombre`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState1({ loading: false, repos: repos });
      });
  }, [setAppState1]);
  
  return (
    <span className="pigeon">
      <h2>Pigeon Information</h2>
      <div className="pigeon-title-info">
        <Button variant="contained"
         onClick={() => setPoppupAjouter(true)}>Ajouter Pigeon</Button>
      </div>

      <table id="customers">
        <thead>
          <tr>
            <th>Pigeon</th>
            <th>Année</th>
            <th>Momber</th>
          </tr>
        </thead>
        <tbody>
        {appState.repos &&
            appState.repos.message &&
            appState.repos.message.map((val, index) => (
              <tr key={index}>
                <td >{val.pigeon}</td>
                <td >{val.annee}</td>
                <td>  
       { appState1.repos &&
            appState1.repos.message &&
              appState1.repos.message.filter((el) => el.id  === val.idMombre)[0].mombre}
            </td>
            </tr>
            ))}
        </tbody>
        {poppupAjouter && (
          <PoppupAjourer
            openPopup={(e) => setPoppupAjouter(e)}
            open={poppupAjouter}
          />
        )}
      </table>
    </span>
  );
}
