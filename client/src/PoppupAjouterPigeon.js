import React, { useState , useEffect} from "react";
import axios from "axios";
import { v4 as uuidv4 } from 'uuid';
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";


export default function PoppupAjouterMember({ openPopup, open }) {
  const [pigeon, setPigeon] = useState("");
  const [annee, setAnnee] = useState("");
  const [idMombre, setIdMombre] = useState("");
  const [id, setId] = useState(uuidv4());

  const handleChangeMombre = (event) => {
    setIdMombre(event.target.value);
  };

  const [appState, setAppState] = useState({
    loading: false,
    repos: null,
  });

  useEffect(() => {
    setAppState({ loading: true });
    const apiUrl = `/api/admin/getmombre`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState({ loading: false, repos: repos });
      });
  }, [setAppState]);


  const handlePigeon = async (e) => {
    e.preventDefault();
      await axios
        .request({
          url: "/api/admin/pigeon",
          method: "POST",
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          data: {id ,pigeon , annee , idMombre }
        })
        .then((res) => openPopup(false))
        .catch((err) => console.log(err))
  };



  return (
    <Modal
      open={open}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <div className="poppupP">
        <div className="poppup-title">
          <h4>Ajouter</h4>
          <div className="poppup-close" onClick={() => openPopup(false)}>
            x
          </div>
        </div>
        <div className="poppup-body">
          <div className="poppup-body-box1">
          <h3>Pigeon :</h3>
          <input
            type="text"
            onChange={(e) => setPigeon(e.target.value)}
            name="pigeon"
            value={pigeon}
            className="input-box"
            placeholder="pigeon"
          />
          </div>

          <div className="poppup-body-box1">
          <h3>Année :</h3>
          <input
            type="text"
            name="annee"
            value={annee}
            className="input-box"
            placeholder="Année"
            onChange={(e) => setAnnee(e.target.value)}
          />
          </div>

          <div className="poppup-body-box1">
          <h3>Mombre :</h3>
          <FormControl sx={{ marginLeft: "5px" , minWidth: 300 }} size="small">
                  <InputLabel id="demo-select-small">Attacher</InputLabel>
                  <Select
                    labelId="demo-select-small"
                    id="demo-select-small"
                    value={idMombre}
                    label="Attacher"
                    onChange={handleChangeMombre}
                  >
                    {appState.repos &&
                      appState.repos.message &&
                      appState.repos.message.map((val, index) => (
                          <MenuItem key={index} value={val.id}>{val.mombre}</MenuItem>
                      ))}
                  </Select>
          </FormControl>
          </div>

          <div className="button-enrgister">
        <Button
          onClick={handlePigeon}
          style={{ width: "200px", backgroundColor: "#306ED9" , color : "#fff"}}
          sx={{ margin: "0 5px -10px 0" }}
          // variant="contained"
        >
          Enregister
        </Button>
          </div>

        </div>
        </div>
    </Modal>
  );
}
