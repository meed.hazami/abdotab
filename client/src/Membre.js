import React , {useState  , useEffect} from "react";
import Button from '@mui/material/Button';
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import PoppupAjourer from "./PoppupAjouterMember";

export default function Membre() {
  const [poppupAjouter, setPoppupAjouter] = useState(false);

  const [appState, setAppState] = useState({
    loading: false,
    repos: null,
  });

  const [appState1, setAppState1] = useState({
    loading: false,
    repos: null,
  });

  useEffect(() => {
    setAppState({ loading: true });
    const apiUrl = `/api/admin/getmombre`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState({ loading: false, repos: repos });
      });
  }, [setAppState, poppupAjouter, setPoppupAjouter]);


  useEffect(() => {
    setAppState1({ loading: true });
    const apiUrl = `/api/admin/getpigeon`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState1({ loading: false, repos: repos });
      });
  }, [setAppState1, poppupAjouter, setPoppupAjouter]);

  return (
    <span className="membre">

      <h2>Menbre Information</h2>

      <div className="membre-title-info">
        <Button variant="contained"
                onClick={() => setPoppupAjouter(true)}
                >Ajouter Membre</Button>
      </div>

      <table id="customers">
      <thead>
        <tr>
          <th>Nom et Prénom</th>
          <th>N</th>
          <th>E</th>
          <th>Pigeon</th>
        </tr>
      </thead>
      <tbody>
      {appState.repos &&
            appState.repos.message &&
            appState.repos.message.map((val, index) => (
              <tr key={index}>
                <td >{val.mombre}</td>
                <td >{val.N}</td>
                <td >{val.E}</td>
                <td >
                <FormControl sx={{ marginLeft: "5px" , minWidth: "100%" }} size="small">
                  <InputLabel id="demo-select-small">pigeons</InputLabel>
                  <Select
                    labelId="demo-select-small"
                    id="demo-select-small"
                    // value={val.mombre}
                    label="Attacher"
                  >
                    {appState1.repos &&
                      appState1.repos.message &&
                      appState1.repos.message.filter((el) => el.idMombre  === val.id)
                                             .map((val, index) => (
                        <MenuItem key={index} value={val.id}>{val.pigeon} {"-"} {val.annee}</MenuItem>
                    ))
                    }
                  </Select>
                </FormControl>
                </td>
              </tr>
            ))}
      </tbody>
      {poppupAjouter && (
          <PoppupAjourer
            openPopup={(e) => setPoppupAjouter(e)}
            open={poppupAjouter}
          />
        )}
      </table>
    </span>
  );
}
