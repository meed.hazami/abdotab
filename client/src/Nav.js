import React from "react";
import "./styles.css";
import { Link } from "react-router-dom";

export default function Nav(){

  return(
        <div className="navbar">
           <ul className="nav-links">
              <Link to="/">Member</Link>
              <Link to="/pigeon">Pigeon</Link>
              <Link to="/councours">Councours</Link>
           </ul>
        </div>
  );

}