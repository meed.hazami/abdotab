import React, { useState } from "react";
import axios from "axios";
import { v4 as uuidv4 } from 'uuid';
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";


export default function PoppupAjouterMember({ openPopup, open }) {
  const [mombre, setMombre] = useState("");
  const [id, setId] = useState(uuidv4());
  const [N, setN] = useState("");
  const [E, setE] = useState("");

  const handleMombre = async (e) => {
    e.preventDefault();
      await axios
        .request({
          url: "/api/admin/mombre",
          method: "POST",
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          data: {id , mombre ,N ,E }
        })
        .then((res) => openPopup(false))
        .catch((err) => console.log(err))
  };

  return (
    <Modal
      open={open}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <div className="poppupM">
        <div className="poppup-title">
          <h4>Ajouter</h4>
          <div className="poppup-close" onClick={() => openPopup(false)}>
            x
          </div>
        </div>
        <div className="poppup-body">
          <div className="poppup-body-box1">
          <h3>Mombre :</h3>
          <input
            type="text"
            onChange={(e) => setMombre(e.target.value)}
            name="mombre"
            value={mombre}
            className="input-box"
            placeholder="Mombre"
          />
          </div>

          <div className="poppup-body-box1">
          <h3>N :</h3>
          <input
            type="text"
            name="N"
            value={N}
            className="input-box"
            placeholder="N"
            onChange={(e) => setN(e.target.value)}
          />
          </div>

          <div className="poppup-body-box1">
          <h3>E :</h3>
          <input 
            type="text"
            name="E"
            value={E}
            className="input-box"
            placeholder="E"
            onChange={(e) => setE(e.target.value)}
            >
          </input>
          </div>
          <div className="button-enrgister">
        <Button
          onClick={handleMombre}
          style={{ width: "200px", backgroundColor: "#306ED9" , color : "#fff"}}
          sx={{ margin: "0 5px -10px 0" }}
          // variant="contained"
        >
          Enregister
        </Button>
          </div>

        </div>
        </div>
    </Modal>
  );
}
