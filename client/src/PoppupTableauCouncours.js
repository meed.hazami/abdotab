import React, { useState, useEffect } from "react";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import { width } from "@mui/system";

export default function PoppupTableauCouncours({
  openPopup,
  open,
  resultat,
  appStatePigeon,
  appStateMombre,
}) {
  const [point, setPoint] = useState("100");
  const [appStateM, setAppStateM] = useState({
    loading: false,
    repos: null,
  });

  const [alertP, setAlertP] = useState({
    loading: false,
    repos: null,
  });

  const fetchE = (name, N, E) => {
    let Ncordonner =
      appStateMombre &&
      appStateMombre.repos &&
      appStateMombre.repos.message.filter(
        (el) =>
          el.id ===
          (appStatePigeon &&
            appStatePigeon.repos &&
            appStatePigeon.repos.message.filter((el) => el.pigeon === name)[0]
              .idMombre)
      )[0].N;

    let Ecordonner =
      appStateMombre &&
      appStateMombre.repos &&
      appStateMombre.repos.message.filter(
        (el) =>
          el.id ===
          (appStatePigeon &&
            appStatePigeon.repos &&
            appStatePigeon.repos.message.filter((el) => el.pigeon === name)[0]
              .idMombre)
      )[0].E;
    return getDistanceFromLatLonInKm(Ncordonner, Ecordonner, N, E);
  };

  const momberTotaleScore =
    resultat &&
    resultat.pigeonId &&
    resultat.pigeonId
      .filter((el) => el.dateArriver !== undefined)
      .sort((a, b) => {
        const distanceA = fetchE(a.name, resultat.N, resultat.E);
        const timeA =
          (new Date(a.dateArriver) -
            new Date(
              resultat.liberation.toString().slice(0, 19).replace("T", " ")
            )) /
          3600000;
        const vitesseA = distanceA / timeA;

        const distanceB = fetchE(b.name, resultat.N, resultat.E);
        const timeB =
          (new Date(b.dateArriver) -
            new Date(
              resultat.liberation.toString().slice(0, 19).replace("T", " ")
            )) /
          3600000;
        const vitesseB = distanceB / timeB;

        return vitesseB - vitesseA;
      })
      .map((val, index) => {
        let PPoint = point;
        const maxV = resultat && resultat.pigeonId && resultat.pigeonId.length;
        if (index > 0) {
          PPoint = (point - (index * 100) / maxV).toFixed(2);
        }
        return {
          name: val.name,
          dateArriver: val.dateArriver,
          momber: val.momber,
          PPoint: PPoint,
        };
      })
      .reduce((acc, curr) => {
        const index = acc.findIndex((el) => el[0]?.momber === curr.momber);
        if (index === -1) {
          acc.push([{ ...curr }]);
        } else {
          acc[index].push({ ...curr });
        }
        return acc;
      }, [])
      .map((vval, iindex) => {
        if (iindex < 5) {
          return {
            momber: vval[0].momber,
            pigeon1: vval.length >= 1 ? vval[0].PPoint : 0,
            pigeon2: vval.length >= 2 ? vval[1].PPoint : 0,
            pigeon3: vval.length >= 3 ? vval[2].PPoint : 0,
            pigeon4: vval.length >= 4 ? vval[3].PPoint : 0,
            pigeon5: vval.length >= 5 ? vval[4].PPoint : 0,
            point: (
              Number(vval.length >= 1 ? vval[0].PPoint : 0) +
              Number(vval.length >= 2 ? vval[1].PPoint : 0) +
              Number(vval.length >= 3 ? vval[2].PPoint : 0) +
              Number(vval.length >= 4 ? vval[3].PPoint : 0) +
              Number(vval.length >= 5 ? vval[4].PPoint : 0)
            ).toFixed(2),
          };
        }
      })
      .sort((a, b) => b.point - a.point);

  const momberTotaleScore3 =
    resultat &&
    resultat.pigeonId &&
    resultat.pigeonId
      .filter((el) => el.dateArriver !== undefined)
      .sort((a, b) => {
        const distanceA = fetchE(a.name, resultat.N, resultat.E);
        const timeA =
          (new Date(a.dateArriver) -
            new Date(
              resultat.liberation.toString().slice(0, 19).replace("T", " ")
            )) /
          3600000;
        const vitesseA = distanceA / timeA;

        const distanceB = fetchE(b.name, resultat.N, resultat.E);
        const timeB =
          (new Date(b.dateArriver) -
            new Date(
              resultat.liberation.toString().slice(0, 19).replace("T", " ")
            )) /
          3600000;
        const vitesseB = distanceB / timeB;

        return vitesseB - vitesseA;
      })
      .map((val, index) => {
        let PPoint = point;
        const maxV = resultat && resultat.pigeonId && resultat.pigeonId.length;
        if (index > 0) {
          PPoint = (point - (index * 100) / maxV).toFixed(2);
        }
        return {
          name: val.name,
          dateArriver: val.dateArriver,
          momber: val.momber,
          PPoint: PPoint,
        };
      })
      .reduce((acc, curr) => {
        const index = acc.findIndex((el) => el[0]?.momber === curr.momber);
        if (index === -1) {
          acc.push([{ ...curr }]);
        } else {
          acc[index].push({ ...curr });
        }
        return acc;
      }, [])
      .map((vval, iindex) => {
        if (iindex < 5) {
          return {
            momber: vval[0].momber,
            pigeon1: vval.length >= 1 ? vval[0].PPoint : 0,
            pigeon2: vval.length >= 2 ? vval[1].PPoint : 0,
            pigeon3: vval.length >= 3 ? vval[2].PPoint : 0,
            point: (
              Number(vval.length >= 1 ? vval[0].PPoint : 0) +
              Number(vval.length >= 2 ? vval[1].PPoint : 0) +
              Number(vval.length >= 3 ? vval[2].PPoint : 0)
            ).toFixed(2),
          };
        }
      })
      .sort((a, b) => b.point - a.point);

  const asPigeon =
    resultat &&
    resultat.pigeonId &&
    resultat.pigeonId
      .filter((el) => el.dateArriver !== undefined)
      .sort((a, b) => {
        const distanceA = fetchE(a.name, resultat.N, resultat.E);
        const timeA =
          (new Date(a.dateArriver) -
            new Date(
              resultat.liberation.toString().slice(0, 19).replace("T", " ")
            )) /
          3600000;
        const vitesseA = distanceA / timeA;

        const distanceB = fetchE(b.name, resultat.N, resultat.E);
        const timeB =
          (new Date(b.dateArriver) -
            new Date(
              resultat.liberation.toString().slice(0, 19).replace("T", " ")
            )) /
          3600000;
        const vitesseB = distanceB / timeB;

        return vitesseB - vitesseA;
      })
      .map((val, index) => {
        let PPoint = point;
        const maxV = resultat && resultat.pigeonId && resultat.pigeonId.length;
        if (index > 0) {
          PPoint = (point - (index * 100) / maxV).toFixed(2);
        }
        return {
          name: val.name,
          dateArriver: val.dateArriver,
          momber: val.momber,
          PPoint: PPoint,
        };
      });

  useEffect(() => {
    setAlertP({ loading: true });
    const apiUrl = `/api/admin/getasPigeon`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAlertP({ loading: false, repos: repos });
      });
  }, [setAlertP]);

  const handleAsPigeon = async (e) => {
    e.preventDefault();
    let dataAsPigeon = asPigeon.map((el) => ({
      id: uuidv4(),
      dateArriver: el.dateArriver,
      name: el.name,
      momber: el.momber,
      point: el.PPoint,
    }));
    await axios
      .request({
        url: "/api/admin/asPigeon",
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        data: {
          dateDebut: resultat.liberation,
          dataAsPigeon,
        },
      })
      .then((res) => window.location.reload())
      .catch((err) => console.log(err));
    console.log("dataAsPigeon", dataAsPigeon);
  };

  function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    const R = 6371; // Rayon de la Terre en km
    const dLat = deg2rad(lat2 - lat1); // Conversion en radian des différences de latitude et de longitude
    const dLon = deg2rad(lon2 - lon1);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(lat1)) *
        Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); // Calcul de l'angle entre les points
    const distance = R * c; // Distance en km
    return distance;
  }

  function deg2rad(deg) {
    return deg * (Math.PI / 180);
  }

  useEffect(() => {
    setAppStateM({ loading: true });
    const apiUrl = `/api/admin/getmombre`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppStateM({ loading: false, repos: repos });
      });
  }, [setAppStateM]);

  return (
    <Modal
      open={open}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <div className="poppupR">
        <div className="poppup-title">
          <h4>Tableau De Resultat</h4>
          <h2>Association tunisienne des pigeons voyageurs Sfax</h2>
          <div className="poppup-close" onClick={() => openPopup(false)}>
            x
          </div>
        </div>
        <div className="poppup-body">
          <div className="box">
            <h3>Point Maximum:</h3>
            <input
              type="number"
              name="point"
              value={point}
              className="input-box"
              placeholder="Point"
              onChange={(e) => setPoint(e.target.value)}
            ></input>
          </div>
          <div
            style={{
              width: "80%",
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
            }}
          >
            <h2>Resultat Pigeon</h2>
            <h2 style={{ color: "#4BAB6E" }}>
              {resultat &&
                String(
                  resultat.pigeonId && resultat.pigeonId[0].dateArriver
                ).split("T")[0]}
            </h2>
          </div>
          <table style={{ marginBottom: "-11px" }} id="customers">
            <thead>
              <tr>
                <th style={{ backgroundColor: "#464646", textAlign: "center" }}>
                  Nom du course
                </th>
                <th style={{ backgroundColor: "#464646", textAlign: "center" }}>
                  Nombre de pigeon
                </th>
                <th style={{ backgroundColor: "#464646", textAlign: "center" }}>
                  Distance
                </th>
                <th style={{ backgroundColor: "#464646", textAlign: "center" }}>
                  N
                </th>
                <th style={{ backgroundColor: "#464646", textAlign: "center" }}>
                  E
                </th>
                <th style={{ backgroundColor: "#464646", textAlign: "center" }}>
                  Membre
                </th>
                <th style={{ backgroundColor: "#464646", textAlign: "center" }}>
                  Date liberation
                </th>
              </tr>
            </thead>
            <tbody>
              <td style={{ textAlign: "center" }}>{resultat.nom}</td>
              <td style={{ textAlign: "center" }}>
                {resultat.pigeonId.length}
              </td>
              <td style={{ textAlign: "center" }}>
                <input
                  type="text"
                 // onChange={(e) => setNom(e.target.value)}
                  name="nom"
                 // value={nom}
                  placeholder="Distence"
                />
              </td>
              <td style={{ textAlign: "center" }}>{resultat.N}</td>
              <td style={{ textAlign: "center" }}>{resultat.E}</td>
              <td style={{ textAlign: "center" }}>{resultat.pigeonId.filter((obj, index) => {
  return index === resultat.pigeonId.findIndex(obj2 => obj2.momber === obj.momber);
}).length}</td>
              <td style={{ textAlign: "center" }}>{resultat.liberation.toString().slice(0, 19).replace("T", " ").substring(11, 19)}</td>
            </tbody>
          </table>
          <table id="customers">
            <thead>
              <tr>
                <th style={{ textAlign: "center" }}>Order</th>
                <th style={{ textAlign: "center" }}>Nom et Prenon</th>
                <th style={{ textAlign: "center" }}>Numéro Pigeon</th>
                <th style={{ textAlign: "center" }}>Départ</th>
                <th style={{ textAlign: "center" }}>Arriver</th>
                <th style={{ textAlign: "center" }}>Durée de Vol</th>
                <th style={{ textAlign: "center" }}>Distance</th>
                <th style={{ textAlign: "center" }}>Vitesse en m/mn</th>
                <th style={{ textAlign: "center" }}>Vitesse en km/h</th>
                <th style={{ textAlign: "center" }}>N.B Point</th>
              </tr>
            </thead>
            {console.log("rrrrrrrr", resultat)}

            <tbody>
              {resultat &&
                resultat.pigeonId &&
                resultat.pigeonId
                  .filter((el) => el.dateArriver !== undefined)
                  .sort((a, b) => {
                    const distanceA = fetchE(a.name, resultat.N, resultat.E);
                    const timeA =
                      (new Date(a.dateArriver) -
                        new Date(
                          resultat.liberation
                            .toString()
                            .slice(0, 19)
                            .replace("T", " ")
                        )) /
                      3600000;
                    const vitesseA = distanceA / timeA;

                    const distanceB = fetchE(b.name, resultat.N, resultat.E);
                    const timeB =
                      (new Date(b.dateArriver) -
                        new Date(
                          resultat.liberation
                            .toString()
                            .slice(0, 19)
                            .replace("T", " ")
                        )) /
                      3600000;
                    const vitesseB = distanceB / timeB;

                    return vitesseB - vitesseA;
                  })
                  .map((val, index) => {
                    let nbPoint = point;
                    const maxV =
                      resultat && resultat.pigeonId && resultat.pigeonId.length;
                    if (index > 0) {
                      nbPoint = (point - (index * 100) / maxV).toFixed(2);
                    }

                    return (
                      <tr key={index}>
                        <td style={{ textAlign: "center" }}>{index + 1}</td>
                        <td style={{ textAlign: "start" }}>{val.momber}</td>
                        <td style={{ textAlign: "center" }}>{val.name}</td>
                        <td style={{ textAlign: "center" }}>
                          {resultat.liberation.substring(11, 19)}
                        </td>
                        <td style={{ textAlign: "center" }}>
                          {val.dateArriver.substring(11, 19)}
                        </td>

                        <td style={{ textAlign: "center" }}>
                          {(
                            (new Date(val.dateArriver) -
                              new Date(
                                resultat.liberation
                                  .toString()
                                  .slice(0, 19)
                                  .replace("T", " ")
                              )) /
                            3600000
                          ).toFixed(0) +
                            "h " +
                            (
                              ((new Date(val.dateArriver) -
                                new Date(
                                  resultat.liberation
                                    .toString()
                                    .slice(0, 19)
                                    .replace("T", " ")
                                )) %
                                3600000) /
                              60000
                            ).toFixed(0) +
                            "min " +
                            (
                              ((new Date(val.dateArriver) -
                                new Date(
                                  resultat.liberation
                                    .toString()
                                    .slice(0, 19)
                                    .replace("T", " ")
                                )) %
                                60000) /
                              1000
                            ).toFixed(0) +
                            "sec "}
                        </td>
                        <td style={{ textAlign: "center" }}>
                          {fetchE(val.name, resultat.N, resultat.E).toFixed(3) +
                            " km"}
                        </td>
                        <td style={{ textAlign: "center" }}>
                          {((( fetchE(val.name, resultat.N, resultat.E) /
                            ((new Date(val.dateArriver) -
                              new Date(
                                resultat.liberation
                                  .toString()
                                  .slice(0, 19)
                                  .replace("T", " ")
                              )) /
                              3600000)
                          )*1000)/60).toFixed(2) +
                            " m/mn"}
                        </td>
                        <td style={{ textAlign: "center" }}>
                          {(
                            fetchE(val.name, resultat.N, resultat.E) /
                            ((new Date(val.dateArriver) -
                              new Date(
                                resultat.liberation
                                  .toString()
                                  .slice(0, 19)
                                  .replace("T", " ")
                              )) /
                              3600000)
                          ).toFixed(2) + " km/h"}
                        </td>
                        <td style={{ textAlign: "center" }}>
                          {nbPoint < 0 ? 0 : nbPoint}
                        </td>
                      </tr>
                    );
                  })}
            </tbody>
          </table>

          <h2 style={{ marginTop: "80px", width: "100%", textAlign: "center" }}>
            Resultat Membre Top 5
          </h2>
          <table id="customers">
            <thead>
              <tr>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Order
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Nom et Prénon
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Pigeon 1
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Pigeon 2
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Pigeon 3
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Pigeon 4
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Pigeon 5
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  N.B Point
                </th>
              </tr>
            </thead>
            <tbody>
              {momberTotaleScore.map((vall, index) => (
                <tr>
                  <td style={{ textAlign: "center" }}>{index + 1}</td>
                  <td style={{ textAlign: "center" }}>{vall.momber}</td>
                  <td style={{ textAlign: "center" }}>{vall.pigeon1}</td>
                  <td style={{ textAlign: "center" }}>{vall.pigeon2}</td>
                  <td style={{ textAlign: "center" }}>{vall.pigeon3}</td>
                  <td style={{ textAlign: "center" }}>{vall.pigeon4}</td>
                  <td style={{ textAlign: "center" }}>{vall.pigeon5}</td>
                  <td style={{ textAlign: "center" }}>{vall.point}</td>
                </tr>
              ))}
            </tbody>
          </table>

          <h2 style={{ marginTop: "80px", width: "100%", textAlign: "center" }}>
            Resultat Membre Top 3
          </h2>
          <table style={{ marginBottom: "30px" }} id="customers">
            <thead>
              <tr>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Order
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Nom et Prénon
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Pigeon 1
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Pigeon 2
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Pigeon 3
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  N.B Point
                </th>
              </tr>
            </thead>
            <tbody>
              {momberTotaleScore3.map((vall, index) => (
                <tr>
                  <td style={{ textAlign: "center" }}>{index + 1}</td>
                  <td style={{ textAlign: "center" }}>{vall.momber}</td>
                  <td style={{ textAlign: "center" }}>{vall.pigeon1}</td>
                  <td style={{ textAlign: "center" }}>{vall.pigeon2}</td>
                  <td style={{ textAlign: "center" }}>{vall.pigeon3}</td>
                  <td style={{ textAlign: "center" }}>{vall.point}</td>
                </tr>
              ))}
            </tbody>
            <Button
              style={{ marginTop: "20px" }}
              variant="contained"
              onClick={handleAsPigeon}
              disabled={
                alertP &&
                alertP.repos &&
                alertP.repos.message.filter(
                  (el) => el.dateDebut === resultat.liberation
                ).length === 1
              }
            >
              Enregistrer ace Pigeon
            </Button>
          </table>
        </div>
      </div>
    </Modal>
  );
}
