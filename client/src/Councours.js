import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import PoppupAjourer from "./PoppupAjouterCouncours";
import PoppupTableauCouncours from "./PoppupTableauCouncours";
import PoppupAjouterDatePigeon from "./PoppupAjouterDatePigeon";

export default function Councours() {
  const [poppupAjouter, setPoppupAjouter] = useState(false);
  const [poppupTableau, setPoppupTableau] = useState(false);
  const [poppupDatePigeon, setPoppupDatePigeon] = useState(false);
  const [resultat, setResultat] = useState({});
  const [infoPigeon, setInfoPigeon] = useState({});
  const [idCourse, setIdCourse] = useState("");

  const [appStatePigeon, setAppStatePigeon] = useState({
    loading: false,
    repos: null,
  });

  const [appState, setAppState] = useState({
    loading: false,
    repos: null,
  });

  const [appState1, setAppState1] = useState({
    loading: false,
    repos: null,
  });

  const [appState2, setAppState2] = useState({
    loading: false,
    repos: null,
  });


  useEffect(() => {
    setAppStatePigeon({ loading: true });
    const apiUrl = `/api/admin/getpigeon`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppStatePigeon({ loading: false, repos: repos });
      });
  }, [setAppStatePigeon]);

  useEffect(() => {
    setAppState2({ loading: true });
    const apiUrl = `/api/admin/getasPigeon`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState2({ loading: false, repos: repos });
      });
  }, [setAppState2]);

   
  useEffect(() => {
    setAppState({ loading: true });
    const apiUrl = `/api/admin/getconcours`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState({ loading: false, repos: repos });
      });
  }, [setAppState, poppupAjouter, setPoppupAjouter]);

  useEffect(() => {
    setAppState1({ loading: true });
    const apiUrl = `/api/admin/getmombre`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState1({ loading: false, repos: repos });
      });
  }, [setAppState1]);


  function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    const R = 6371; // Rayon de la Terre en km
    const dLat = deg2rad(lat2-lat1);  // Conversion en radian des différences de latitude et de longitude
    const dLon = deg2rad(lon2-lon1);
    const a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ;
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); // Calcul de l'angle entre les points
    const distance = R * c; // Distance en km
    return distance;
  }
  
  function deg2rad(deg) {
    return deg * (Math.PI/180)
  }
  
  // Exemple d'utilisation de la fonction
  //const distance = getDistanceFromLatLonInKm(34.767608, 10.764537,40.7128,-74.0060);
  //console.log(distance + " km"); // Affiche "5836.563902665088 km"

  return (
    <span className="pigeon">
      <h2>Concours Information</h2>
      <div className="pigeon-title-info">
        <Button variant="contained" onClick={() => setPoppupAjouter(true)}>
          Ajouter Concour
        </Button>
      </div>

      <table id="customers">
        <thead>
          <tr>
            <th rowspan="2" style={{ textAlign: "center" }}>
              Nom
            </th>
            <th rowspan="2" style={{ textAlign: "center" }}>
              Liberation
            </th>
            <th rowspan="2" style={{ textAlign: "center" }}>
              Nombre pigeon
            </th>
            <th colspan="2" style={{ textAlign: "center" }}>
              Localisation
            </th>
            <th rowspan="2" style={{ textAlign: "center" }}>
              Pigeon participant
            </th>
            {/* <th rowspan="2" style={{ textAlign: "center" }}>
              Distance
            </th> */}
          </tr>
          <tr>
            <th style={{ textAlign: "center" }}>N</th>
            <th style={{ textAlign: "center" }}>E</th>
          </tr>
        </thead>
        <tbody>
          {appState.repos &&
            appState.repos.message &&
            appState.repos.message.map((val, index) => (
              <tr  key={index}>
                <td onClick={() => {setPoppupTableau(true) , setResultat(val)}} style={{ textAlign: "center" }}>{val.nom}</td>
                <td style={{ textAlign: "center" }}>{val.liberation}</td>
                <td style={{ textAlign: "center" }}>{val && val.pigeonId.length}</td>

                <td style={{ textAlign: "center" }}>{val.N}</td>
                <td style={{ textAlign: "center" }}>{val.E}</td>
                <td style={{ textAlign: "center" }}>
                  <FormControl
                    sx={{ marginLeft: "5px", minWidth: 300 }}
                    size="small"
                  >
                    <InputLabel id="demo-select-small">Pigeons</InputLabel>
                    <Select labelId="demo-select-small" id="demo-select-small">
                      {val &&
                        val.pigeonId &&
                        val.pigeonId.map((val1, index1) => (
                          <MenuItem onClick={() => {setPoppupDatePigeon(true) , setInfoPigeon(val1) , setIdCourse(val._id) }}  key={index1} value={val1.name}>
                            {val1.name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                </td>
                {/* <td style={{ textAlign: "center" }}> {getDistanceFromLatLonInKm(34.7673565, 10.7647528,val.N,val.E).toFixed(3) + " " +"Km" }</td> */}
              </tr>
            ))}
        </tbody>
        {poppupAjouter && (
          <PoppupAjourer
            openPopup={(e) => setPoppupAjouter(e)}
            open={poppupAjouter}
          />
        )}
       {poppupTableau && (
          <PoppupTableauCouncours
            openPopup={(e) => setPoppupTableau(e)}
            open={poppupTableau}
            resultat={resultat}
            appStatePigeon={appStatePigeon}
            appStateMombre={appState1}
          />
        )}
        {poppupDatePigeon && (
          <PoppupAjouterDatePigeon
            openPopup={(e) => setPoppupDatePigeon(e)}
            open={poppupDatePigeon}
            PigeonInfo={infoPigeon}
            idCourse={idCourse}
          />
        )}



      </table>

          <h2 style={{ marginTop: "80px", width: "100%", textAlign: "center" }}>
            Resultat Ace Pigeon
          </h2>
          <table style={{ marginBottom: "30px" }} id="customers">
            <thead>
              <tr>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Order
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Nom et Prénon
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  Pigeon
                </th>
                <th style={{ textAlign: "center", backgroundColor: "#2d3e75" }}>
                  N.B Point
                </th>
              </tr>
            </thead>
            <tbody>
              {
              appState2.repos &&
              appState2.repos.message &&
              appState2.repos.message
              .reduce((acc, curr) => {
                curr.dataAsPigeon.forEach((pigeon) => {
                  const index = acc.findIndex((elem) => elem.name === pigeon.name);
                  if (index !== -1) {
                    acc[index].point = (Number(acc[index].point) + Number(pigeon.point)).toString();
                  } else {
                    acc.push({name: pigeon.name ,  mombre: pigeon.momber, point: pigeon.point });
                  }
                });
                return acc;
              }, []).sort((a, b) => b.point - a.point)
              .map((vall, index) => (
                <tr>
                  <td style={{ textAlign: "center" }}>{index + 1}</td>
                  <td style={{ textAlign: "center" }}>{vall.mombre}</td>
                  <td style={{ textAlign: "center" }}>{vall.name}</td>
                  <td style={{ textAlign: "center" }}>{vall.point}</td>
                </tr>
              ))}
            </tbody>
          </table>
    </span>
  );
}
