import React, { useState , useEffect} from "react";
import axios from "axios";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";


export default function PoppupAjouterDatePigeon({ openPopup, open , PigeonInfo , idCourse}) {
  const [dateArriver, setDateArriver] = useState("");
  const [appStateP, setAppStateP] = useState({
    loading: false,
    repos: null,
  });
  const [appStateM, setAppStateM] = useState({
    loading: false,
    repos: null,
  });

  const handlePigeon = async (e) => {
    e.preventDefault();
      await axios
        .request({
          url: "/api/admin/dateArriver",
          method: "POST",
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          data: {idCourse ,  PigeonInfo :PigeonInfo.name , momber : appStateM.repos && appStateM.repos.message
            .filter((val) => val.id === (appStateP.repos &&
            appStateP.repos.message.filter((el) => el.pigeon  === PigeonInfo.name)[0].idMombre ))[0] && 
            appStateM.repos && appStateM.repos.message
            .filter((val) => val.id === (appStateP.repos &&
            appStateP.repos.message.filter((el) => el.pigeon  === PigeonInfo.name)[0].idMombre ))[0].mombre, dateArriver}
        })
        .then((res) => window.location.reload())
        .catch((err) => console.log(err))
  };

  useEffect(() => {
    setAppStateM({ loading: true });
    const apiUrl = `/api/admin/getmombre`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppStateM({ loading: false, repos: repos });
      });
  }, [setAppStateM]);

  useEffect(() => {
    setAppStateP({ loading: true });
    const apiUrl = `/api/admin/getpigeon`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppStateP({ loading: false, repos: repos });
      });
  }, [setAppStateP]);


  return (
    <Modal
      open={open}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <div className="poppupDP">
        <div className="poppup-title">
          <h4>Ajouter Date Arriver de Pigeon  { PigeonInfo.name}</h4>
          <div className="poppup-close" onClick={() => openPopup(false)}>
            x
          </div>
        </div>
        <div className="poppup-body">
          <div className="poppup-body-box1">
          <h3>Date Arriver :</h3>
          <input
            type="datetime-local"
            name="liberation"
            value={dateArriver}
            className="input-box"
            placeholder="Enter Date Arriver"
            onChange={(e) => setDateArriver(e.target.value)}
            step="1"
            pattern="\d{2}-\d{2}-\d{4}T\d{2}:\d{2}:\d{2}"
          />
          </div>
          <div className="button-enrgister">
        <Button
          onClick={handlePigeon}
          style={{ width: "200px", backgroundColor: "#306ED9" , color : "#fff"}}
          sx={{ margin: "0 5px -10px 0" }}
          // variant="contained"
        >
          Enregister
        </Button>
          </div>

        </div>
        </div>
    </Modal>
  );
}
