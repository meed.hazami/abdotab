import './App.css';
import Pigeon from "./Pigeon";
import Councours from "./Councours";
import Nav from "./Nav";
import Membre from "./Membre";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

function App() {
  return (
    <Router>
      <div className="App">
        <Nav />
        <Routes>
          <Route path="/" exact element={<Membre />} />
          <Route path="/pigeon" element={<Pigeon />} />
          <Route path="/councours" element={<Councours />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
