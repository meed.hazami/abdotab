const Mombre = require("../models/mombre.model");
const Pigeon = require("../models/pigeon.model ");
const Concours = require("../models/concours.model");
const AsPigeon = require("../models/asPigeon.model");



module.exports.dateArriver = async (req, res) => {

try {
      const { idCourse, PigeonInfo, momber, dateArriver } = req.body;
      const result = await Concours.findOneAndUpdate(
        { _id: idCourse, "pigeonId.name": PigeonInfo },
        { $set: { "pigeonId.$.dateArriver": dateArriver ,
                  "pigeonId.$.momber": momber} },
        { new: true }
      );
      res.status(200).send({ message: result });
    } catch (err) {
      res.status(500).send({ err });
    }
};

module.exports.asPigeon = async (req, res) => {
  try {
    const { dataAsPigeon , dateDebut } = req.body;

    const pig = await AsPigeon.findOne({ "dateDebut": dateDebut });
    if (pig) return res.status(400).send({ message: "AsPigeon Save already" });

    const result = await AsPigeon.create({dateDebut, dataAsPigeon });
    res.status(201).send({ message: result });
  } catch (err) {
    res.status(500).send({ err });
  }
};

module.exports.readAsPigeon = async (req, res) => {
  try {
    const resultat = await AsPigeon.find({});
    res.status(201).send({ message: resultat });
  } catch (err) {
    res.status(500).send({ err });
  }
};

module.exports.addConcours = async (req, res) => {
  try {
    const {id, nom, liberation , pigeonId , N , E } = req.body;
    const result = await Concours.create({
      id,
      nom,
      liberation,
      pigeonId,
      N,
      E
    });
    res.status(201).send({ message: result });
  } catch (err) {
    res.status(500).send({ err });
  }
};

module.exports.readConcours = async (req, res) => {
  try {
    const resultat = await Concours.find({});
    res.status(201).send({ message: resultat });
  } catch (err) {
    res.status(500).send({ err });
  }
};

module.exports.addMombre = async (req, res) => {
  try {
    const {id, mombre, N , E } = req.body;
    const Monber = await Mombre.create({
      id,
      mombre,
      N,
      E
    });
    res.status(201).send({ message: Monber });
  } catch (err) {
    res.status(500).send({ err });
  }
};



module.exports.readPostMombre = async (req, res) => {
  try {
    const resultat = await Mombre.find({});
    res.status(201).send({ message: resultat });
  } catch (err) {
    res.status(500).send({ err });
  }
};






module.exports.addPigeon = async (req, res) => {
  try {
    const {id, pigeon, annee, idMombre } = req.body;
    const resl = await Pigeon.create({
      id,
      pigeon,
      annee,
      idMombre
    });
    const updateMombre = await Mombre.findOneAndUpdate(
      { id: idMombre },
      {
        $push: {
          pigeon: id,
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true }
    );
    res.status(201).send({ message: updateMombre });
  } catch (err) {
    res.status(500).send({ err });
  }
};

module.exports.readPostPigeon = async (req, res) => {
  try {
    const resultat1 = await Pigeon.find({});
    res.status(201).send({ message: resultat1 });
  } catch (err) {
    res.status(500).send({ err });
  }
};
