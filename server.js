const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const adminRoutes = require('./routes/admin.routes');
require('dotenv').config({path: './config/.env'});
require('./config/db');
const session = require('cookie-session');
const cors = require('cors');
const port = process.env.PORT || 5000;

const app = express();

const http = require('http').Server(app);


app.use(cors());
app.enable('trust proxy');
app.use(function(request, response, next) {
  if (process.env.NODE_ENV != 'development' && !request.secure) {
     return response.redirect("https://" + request.headers.host + request.url);
  }
  next();
})


app.use(
  session({
    secret: "some secret",
    httpOnly: true,  
    secure: true,
  })
)

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

app.use(express.static("client/build"));

////////////////////////////////////////////////////////////////////////////////////////////


app.get('/', (req , res)=>{
  res.send("welcome")
});

// routes
app.use('/api/admin', adminRoutes);


//server
http.listen(port, () => {
  console.log(`Listening on port ${port}`);
})


