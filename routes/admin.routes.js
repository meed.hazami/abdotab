const router = require("express").Router();
const admin = require("../controllers/admin.controller");

router.post("/mombre", admin.addMombre);
router.get('/getmombre', admin.readPostMombre);

router.post("/pigeon", admin.addPigeon);
router.get('/getpigeon', admin.readPostPigeon);

router.post("/concours", admin.addConcours);
router.get('/getconcours', admin.readConcours);

router.post('/dateArriver', admin.dateArriver);

router.post("/asPigeon", admin.asPigeon);
router.get('/getasPigeon', admin.readAsPigeon);
module.exports = router;
