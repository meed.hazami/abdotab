const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
  {
    dateDebut:{
      type: String,
      default: "",
      trim: true,
    },
    dataAsPigeon: {
      type: [Object],
    id: {
      type: String,
      default: "",
      trim: true,
    },
    name: {
      type: String,
      default: "",
      trim: true,
    },
    momber: {
      type: String,
      default: "",
      trim: true,
    },
    point: {
      type: String,
      default: "",
      trim: true,
    }
  }
  },
  {
    timestamps: true,
  }
);

const AsPigeonModel = mongoose.model("asPigeon", userSchema);

module.exports = AsPigeonModel;


// type: [String],