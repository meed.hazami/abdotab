const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
  {
    id: {
      type: String,
      default: "",
      trim: true,
    },
    pigeon: {
      type: String,
      default: "",
      trim: true,
    },
    annee: {
      type: String,
      default: "",
      trim: true,
    },
    idMombre: {
      type: String,
      default: "",
      trim: true,
    }
  },
  {
    timestamps: true,
  }
);

const CategoryModel = mongoose.model("Pigeon", userSchema);

module.exports = CategoryModel;