const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
  {
    id: {
      type: String,
      default: "",
      trim: true,
    },
    pigeon: {
      type: [String]
    },
    mombre: {
      type: String,
      default: "",
      trim: true,
    },
    N: {
      type: String,
      default: "",
      trim: true,
    },
    E: {
      type: String,
      default: "",
      trim: true,
    },

  },
  {
    timestamps: true,
  }
);

const CategoryModel = mongoose.model("Mombre", userSchema);

module.exports = CategoryModel;