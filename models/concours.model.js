const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
  {
    id: {
      type: String,
      default: "",
      trim: true,
    },
    nom: {
      type: String,
      default: "",
      trim: true,
    },
    liberation: {
      type: Date,
      default: "",
      trim: true,
    },
    N: {
      type: String,
      default: "",
      trim: true,
    },
    E: {
      type: String,
      default: "",
      trim: true,
    },
    pigeonId: {
      type: [Object],
      id: {
        type: String,
        default: ""
      },
      momber: {
        type: String,
        default: ""
      },
      dateArriver: {
        type: Date,
        default: null
      }
    },
  },
  {
    timestamps: true,
  }
);

const ConcoursModel = mongoose.model("Concours", userSchema);

module.exports = ConcoursModel;


// type: [String],